# GitLab Runner in Gitpod

A quick way to **install**, **register**, and **run** a [GitLab Runner](https://docs.gitlab.com/runner/) in [Gitpod](https://gitpod.io/).

## How to set up

On GitLab:
1. Fork this project
1. Create a shared, group, or project runner.
1. Copy the generated token `glrt...`

On Gitpod:
1. Add this repository as project
1. Add a project-specific variable with the token called `RUNNER_TOKEN`
1. Make sure the variable is not hidden in workspaces

Everytime you need a GitLab Runner for this project, you can open the repository in Gitpod.

Optionally, you can use the new [Gitpod CLI](https://www.gitpod.io/changelog/gitpod-cli) to get the GitLab runner up and running in seconds.

```sh
gitpod ws create gitlab.com/:repository/gitlab-runner
```
